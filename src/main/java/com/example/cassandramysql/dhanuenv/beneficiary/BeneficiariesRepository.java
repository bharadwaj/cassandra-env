package com.example.cassandramysql.dhanuenv.beneficiary;

import com.example.cassandramysql.dhanuenv.beneficiary.Beneficiaries;
import org.springframework.data.cassandra.repository.CassandraRepository;

public interface BeneficiariesRepository extends CassandraRepository<Beneficiaries, String> {

}
