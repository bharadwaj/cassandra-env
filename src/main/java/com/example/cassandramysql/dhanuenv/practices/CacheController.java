package com.example.cassandramysql.dhanuenv.practices;

import com.example.cassandramysql.dhanuenv.beneficiary.BeneficiariesRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class CacheController {

    @Autowired
    private MySqlUserRepository mySqlUserRepository;

    @Autowired
    private BeneficiariesRepository beneficiariesRepository;

    private static final Logger logger = LogManager.getLogger(CacheController.class);

    @CachePut(value = "mysql", key = "#hm.firstName")
    @PostMapping("mysql")
    public MySqlUser saveToMySql(@RequestBody MySqlUser hm){
        return mySqlUserRepository.save(hm);
    }

    @GetMapping("mysql")
    public ResponseEntity allHiMysqls(){
        return new ResponseEntity<>(mySqlUserRepository.findAll(), HttpStatus.OK);
    }

    @Cacheable(value = "mysql", key = "#firstName")
    @GetMapping("mysql/{firstName}")
    public MySqlUser nameByHiMysql(@PathVariable("firstName") String firstName){
        logger.info("Coming from program.");
        return mySqlUserRepository.findByFirstName(firstName);
    }

    @GetMapping("NotFound")
    public ResponseEntity errorCode(){
        return new ResponseEntity<>(new ErrorMessage("400012", "This is the standard error"),
                HttpStatus.BAD_REQUEST);
    }
}

