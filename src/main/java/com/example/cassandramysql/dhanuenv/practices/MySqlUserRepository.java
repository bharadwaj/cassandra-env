package com.example.cassandramysql.dhanuenv.practices;

import org.springframework.data.repository.CrudRepository;


public interface MySqlUserRepository extends CrudRepository<MySqlUser, Long> {
    MySqlUser findByFirstName(String firstName);
}
