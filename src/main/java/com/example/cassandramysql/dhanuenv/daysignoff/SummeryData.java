package com.example.cassandramysql.dhanuenv.daysignoff;

public class SummeryData {
    private String group;

    private String[] diseases;

    public String getGroup ()
    {
        return group;
    }

    public void setGroup (String group)
    {
        this.group = group;
    }

    public String[] getDiseases ()
    {
        return diseases;
    }

    public void setDiseases (String[] diseases)
    {
        this.diseases = diseases;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [group = "+group+", diseases = "+diseases+"]";
    }
}
