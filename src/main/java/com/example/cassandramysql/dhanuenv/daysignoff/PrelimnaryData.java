package com.example.cassandramysql.dhanuenv.daysignoff;

public class PrelimnaryData {
    private String nameTeacher;

    private String ageInYears;

    private String ClassSection;

    private String districtName;

    private String schoolName;

    private String revenueDivision;

    private String institutionType;

    private String TeacherContact;

    private String schoolID;

    private String registeredAtMobile;

    public String getNameTeacher ()
    {
        return nameTeacher;
    }

    public void setNameTeacher (String nameTeacher)
    {
        this.nameTeacher = nameTeacher;
    }

    public String getAgeInYears ()
    {
        return ageInYears;
    }

    public void setAgeInYears (String ageInYears)
    {
        this.ageInYears = ageInYears;
    }

    public String getClassSection ()
    {
        return ClassSection;
    }

    public void setClassSection (String ClassSection)
    {
        this.ClassSection = ClassSection;
    }

    public String getDistrictName ()
    {
        return districtName;
    }

    public void setDistrictName (String districtName)
    {
        this.districtName = districtName;
    }

    public String getSchoolName ()
    {
        return schoolName;
    }

    public void setSchoolName (String schoolName)
    {
        this.schoolName = schoolName;
    }

    public String getRevenueDivision ()
    {
        return revenueDivision;
    }

    public void setRevenueDivision (String revenueDivision)
    {
        this.revenueDivision = revenueDivision;
    }

    public String getInstitutionType ()
    {
        return institutionType;
    }

    public void setInstitutionType (String institutionType)
    {
        this.institutionType = institutionType;
    }

    public String getTeacherContact ()
    {
        return TeacherContact;
    }

    public void setTeacherContact (String TeacherContact)
    {
        this.TeacherContact = TeacherContact;
    }

    public String getSchoolID ()
    {
        return schoolID;
    }

    public void setSchoolID (String schoolID)
    {
        this.schoolID = schoolID;
    }

    public String getRegisteredAtMobile ()
    {
        return registeredAtMobile;
    }

    public void setRegisteredAtMobile (String registeredAtMobile)
    {
        this.registeredAtMobile = registeredAtMobile;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [nameTeacher = "+nameTeacher+", ageInYears = "+ageInYears+", ClassSection = "+ClassSection+", districtName = "+districtName+", schoolName = "+schoolName+", revenueDivision = "+revenueDivision+", institutionType = "+institutionType+", TeacherContact = "+TeacherContact+", schoolID = "+schoolID+", registeredAtMobile = "+registeredAtMobile+"]";
    }
}

