package com.example.cassandramysql.dhanuenv.daysignoff;

import java.util.ArrayList;

public class DaySignOff
{
    private String recordId;

    private String sessionId;

    private String microPlanId;

    private ArrayList<BeneficiaryData> beneficiaryData;

    private String mht_id;

    public String getRecordId ()
    {
        return recordId;
    }

    public void setRecordId (String recordId)
    {
        this.recordId = recordId;
    }

    public String getSessionId ()
    {
        return sessionId;
    }

    public void setSessionId (String sessionId)
    {
        this.sessionId = sessionId;
    }

    public String getMicroPlanId ()
    {
        return microPlanId;
    }

    public void setMicroPlanId (String microPlanId)
    {
        this.microPlanId = microPlanId;
    }

    public ArrayList<BeneficiaryData> getBeneficiaryData() {
        return beneficiaryData;
    }

    public void setBeneficiaryData(ArrayList<BeneficiaryData> beneficiaryData) {
        this.beneficiaryData = beneficiaryData;
    }

    public String getMht_id ()
    {
        return mht_id;
    }

    public void setMht_id (String mht_id)
    {
        this.mht_id = mht_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [recordId = "+recordId+", sessionId = "+sessionId+", microPlanId = "+microPlanId+", beneficiaryData = "+beneficiaryData+", mht_id = "+mht_id+"]";
    }
}
