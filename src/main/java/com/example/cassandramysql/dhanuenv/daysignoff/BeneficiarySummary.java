package com.example.cassandramysql.dhanuenv.daysignoff;

public class BeneficiarySummary
{
    private String screenedDate;

    private String observation;

    private String others;

    private Drugs[] drugs;

    private String diabetes;

    private String[] eligibleReferrals;

    private String provisionalDiagnosis;

    private String isrefered;

    private String referralFacilityHospital;

    private SummeryData[] summeryData;

    private ReferralWithDiseases[] referralWithDiseases;

    private String[] referralHospital;

    private String chiefComplaints;

    private String hemoglobin;

    private String recommendations;

    private String advice;

    public String getScreenedDate ()
    {
        return screenedDate;
    }

    public void setScreenedDate (String screenedDate)
    {
        this.screenedDate = screenedDate;
    }

    public String getObservation ()
    {
        return observation;
    }

    public void setObservation (String observation)
    {
        this.observation = observation;
    }

    public String getOthers ()
    {
        return others;
    }

    public void setOthers (String others)
    {
        this.others = others;
    }

    public Drugs[] getDrugs ()
    {
        return drugs;
    }

    public void setDrugs (Drugs[] drugs)
    {
        this.drugs = drugs;
    }

    public String getDiabetes ()
    {
        return diabetes;
    }

    public void setDiabetes (String diabetes)
    {
        this.diabetes = diabetes;
    }

    public String[] getEligibleReferrals ()
    {
        return eligibleReferrals;
    }

    public void setEligibleReferrals (String[] eligibleReferrals)
    {
        this.eligibleReferrals = eligibleReferrals;
    }

    public String getProvisionalDiagnosis ()
    {
        return provisionalDiagnosis;
    }

    public void setProvisionalDiagnosis (String provisionalDiagnosis)
    {
        this.provisionalDiagnosis = provisionalDiagnosis;
    }

    public String getIsrefered ()
    {
        return isrefered;
    }

    public void setIsrefered (String isrefered)
    {
        this.isrefered = isrefered;
    }

    public String getReferralFacilityHospital ()
    {
        return referralFacilityHospital;
    }

    public void setReferralFacilityHospital (String referralFacilityHospital)
    {
        this.referralFacilityHospital = referralFacilityHospital;
    }

    public SummeryData[] getSummeryData ()
    {
        return summeryData;
    }

    public void setSummeryData (SummeryData[] summeryData)
    {
        this.summeryData = summeryData;
    }

    public ReferralWithDiseases[] getReferralWithDiseases ()
    {
        return referralWithDiseases;
    }

    public void setReferralWithDiseases (ReferralWithDiseases[] referralWithDiseases)
    {
        this.referralWithDiseases = referralWithDiseases;
    }

    public String[] getReferralHospital ()
    {
        return referralHospital;
    }

    public void setReferralHospital (String[] referralHospital)
    {
        this.referralHospital = referralHospital;
    }

    public String getChiefComplaints ()
    {
        return chiefComplaints;
    }

    public void setChiefComplaints (String chiefComplaints)
    {
        this.chiefComplaints = chiefComplaints;
    }

    public String getHemoglobin ()
    {
        return hemoglobin;
    }

    public void setHemoglobin (String hemoglobin)
    {
        this.hemoglobin = hemoglobin;
    }

    public String getRecommendations ()
    {
        return recommendations;
    }

    public void setRecommendations (String recommendations)
    {
        this.recommendations = recommendations;
    }

    public String getAdvice ()
    {
        return advice;
    }

    public void setAdvice (String advice)
    {
        this.advice = advice;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [screenedDate = "+screenedDate+", observation = "+observation+", others = "+others+", drugs = "+drugs+", diabetes = "+diabetes+", eligibleReferrals = "+eligibleReferrals+", provisionalDiagnosis = "+provisionalDiagnosis+", isrefered = "+isrefered+", referralFacilityHospital = "+referralFacilityHospital+", summeryData = "+summeryData+", referralWithDiseases = "+referralWithDiseases+", referralHospital = "+referralHospital+", chiefComplaints = "+chiefComplaints+", hemoglobin = "+hemoglobin+", recommendations = "+recommendations+", advice = "+advice+"]";
    }
}
