package com.example.cassandramysql.dhanuenv.daysignoff;

public class ScreeningData {
    private QuestionAndAnswer[] questionAndAnswer;

    public QuestionAndAnswer[] getQuestionAndAnswer ()
    {
        return questionAndAnswer;
    }

    public void setQuestionAndAnswer (QuestionAndAnswer[] questionAndAnswer)
    {
        this.questionAndAnswer = questionAndAnswer;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [questionAndAnswer = "+questionAndAnswer+"]";
    }
}
