package com.example.cassandramysql.dhanuenv.daysignoff;

public class PersonalInfo {
    private String mothername;

    private String childname;

    private String contact_no;

    private String fathername;

    private String gender;

    private String dateofbirth;

    private String mcts_no;

    private String uniqueId;

    private String aadhar_no;

    public String getMothername ()
    {
        return mothername;
    }

    public void setMothername (String mothername)
    {
        this.mothername = mothername;
    }

    public String getChildname ()
    {
        return childname;
    }

    public void setChildname (String childname)
    {
        this.childname = childname;
    }

    public String getContact_no ()
    {
        return contact_no;
    }

    public void setContact_no (String contact_no)
    {
        this.contact_no = contact_no;
    }

    public String getFathername ()
    {
        return fathername;
    }

    public void setFathername (String fathername)
    {
        this.fathername = fathername;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getDateofbirth ()
    {
        return dateofbirth;
    }

    public void setDateofbirth (String dateofbirth)
    {
        this.dateofbirth = dateofbirth;
    }

    public String getMcts_no ()
    {
        return mcts_no;
    }

    public void setMcts_no (String mcts_no)
    {
        this.mcts_no = mcts_no;
    }

    public String getUniqueId ()
    {
        return uniqueId;
    }

    public void setUniqueId (String uniqueId)
    {
        this.uniqueId = uniqueId;
    }

    public String getAadhar_no ()
    {
        return aadhar_no;
    }

    public void setAadhar_no (String aadhar_no)
    {
        this.aadhar_no = aadhar_no;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [mothername = "+mothername+", childname = "+childname+", contact_no = "+contact_no+", fathername = "+fathername+", gender = "+gender+", dateofbirth = "+dateofbirth+", mcts_no = "+mcts_no+", uniqueId = "+uniqueId+", aadhar_no = "+aadhar_no+"]";
    }
}