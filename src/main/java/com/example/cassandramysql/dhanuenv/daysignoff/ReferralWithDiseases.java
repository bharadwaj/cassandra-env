package com.example.cassandramysql.dhanuenv.daysignoff;

public class ReferralWithDiseases {
    private String group;

    private String disease;

    private String referrals;

    public String getGroup ()
    {
        return group;
    }

    public void setGroup (String group)
    {
        this.group = group;
    }

    public String getDisease ()
    {
        return disease;
    }

    public void setDisease (String disease)
    {
        this.disease = disease;
    }

    public String getReferrals ()
    {
        return referrals;
    }

    public void setReferrals (String referrals)
    {
        this.referrals = referrals;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [group = "+group+", disease = "+disease+", referrals = "+referrals+"]";
    }
}
