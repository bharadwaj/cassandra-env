package com.example.cassandramysql.dhanuenv.daysignoff;

public class Vitals {
    private String expansionChest;

    private String rightViewVision;

    private String weight;

    private String height;

    private String rightEyeVision;

    private String leftEyeVision;

    private String bmiClassification;

    private String leftViewVision;

    private String BMIValue;

    private String BpValue;

    private String BpSystolicDiatolic;

    private String compressChest;

    public String getExpansionChest ()
    {
        return expansionChest;
    }

    public void setExpansionChest (String expansionChest)
    {
        this.expansionChest = expansionChest;
    }

    public String getRightViewVision ()
    {
        return rightViewVision;
    }

    public void setRightViewVision (String rightViewVision)
    {
        this.rightViewVision = rightViewVision;
    }

    public String getWeight ()
    {
        return weight;
    }

    public void setWeight (String weight)
    {
        this.weight = weight;
    }

    public String getHeight ()
    {
        return height;
    }

    public void setHeight (String height)
    {
        this.height = height;
    }

    public String getRightEyeVision ()
    {
        return rightEyeVision;
    }

    public void setRightEyeVision (String rightEyeVision)
    {
        this.rightEyeVision = rightEyeVision;
    }

    public String getLeftEyeVision ()
    {
        return leftEyeVision;
    }

    public void setLeftEyeVision (String leftEyeVision)
    {
        this.leftEyeVision = leftEyeVision;
    }

    public String getBmiClassification ()
    {
        return bmiClassification;
    }

    public void setBmiClassification (String bmiClassification)
    {
        this.bmiClassification = bmiClassification;
    }

    public String getLeftViewVision ()
    {
        return leftViewVision;
    }

    public void setLeftViewVision (String leftViewVision)
    {
        this.leftViewVision = leftViewVision;
    }

    public String getBMIValue ()
    {
        return BMIValue;
    }

    public void setBMIValue (String BMIValue)
    {
        this.BMIValue = BMIValue;
    }

    public String getBpValue ()
    {
        return BpValue;
    }

    public void setBpValue (String BpValue)
    {
        this.BpValue = BpValue;
    }

    public String getBpSystolicDiatolic ()
    {
        return BpSystolicDiatolic;
    }

    public void setBpSystolicDiatolic (String BpSystolicDiatolic)
    {
        this.BpSystolicDiatolic = BpSystolicDiatolic;
    }

    public String getCompressChest ()
    {
        return compressChest;
    }

    public void setCompressChest (String compressChest)
    {
        this.compressChest = compressChest;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [expansionChest = "+expansionChest+", rightViewVision = "+rightViewVision+", weight = "+weight+", height = "+height+", rightEyeVision = "+rightEyeVision+", leftEyeVision = "+leftEyeVision+", bmiClassification = "+bmiClassification+", leftViewVision = "+leftViewVision+", BMIValue = "+BMIValue+", BpValue = "+BpValue+", BpSystolicDiatolic = "+BpSystolicDiatolic+", compressChest = "+compressChest+"]";
    }
}