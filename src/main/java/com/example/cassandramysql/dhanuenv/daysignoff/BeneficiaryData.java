package com.example.cassandramysql.dhanuenv.daysignoff;

public class BeneficiaryData
{
    private Vitals vitals;

    private String recordId;

    private BeneficiarySummary beneficiarySummary;

    private PersonalInfo personalInfo;

    private ScreeningData screeningData;

    private String attendance;

    private PrelimnaryData prelimnaryData;

    private String beneficiaryID;

    public Vitals getVitals ()
    {
        return vitals;
    }

    public void setVitals (Vitals vitals)
    {
        this.vitals = vitals;
    }

    public String getRecordId ()
    {
        return recordId;
    }

    public void setRecordId (String recordId)
    {
        this.recordId = recordId;
    }

    public BeneficiarySummary getBeneficiarySummary ()
    {
        return beneficiarySummary;
    }

    public void setBeneficiarySummary (BeneficiarySummary beneficiarySummary)
    {
        this.beneficiarySummary = beneficiarySummary;
    }

    public PersonalInfo getPersonalInfo ()
    {
        return personalInfo;
    }

    public void setPersonalInfo (PersonalInfo personalInfo)
    {
        this.personalInfo = personalInfo;
    }

    public ScreeningData getScreeningData ()
    {
        return screeningData;
    }

    public void setScreeningData (ScreeningData screeningData)
    {
        this.screeningData = screeningData;
    }

    public String getAttendance ()
    {
        return attendance;
    }

    public void setAttendance (String attendance)
    {
        this.attendance = attendance;
    }

    public PrelimnaryData getPrelimnaryData ()
    {
        return prelimnaryData;
    }

    public void setPrelimnaryData (PrelimnaryData prelimnaryData)
    {
        this.prelimnaryData = prelimnaryData;
    }

    public String getBeneficiaryID ()
    {
        return beneficiaryID;
    }

    public void setBeneficiaryID (String beneficiaryID)
    {
        this.beneficiaryID = beneficiaryID;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [vitals = "+vitals+", recordId = "+recordId+", beneficiarySummary = "+beneficiarySummary+", personalInfo = "+personalInfo+", screeningData = "+screeningData+", attendance = "+attendance+", prelimnaryData = "+prelimnaryData+", beneficiaryID = "+beneficiaryID+"]";
    }
}

