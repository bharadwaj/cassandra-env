package com.example.cassandramysql.dhanuenv.daysignoff;

public class Drugs {
    private String drugCatgory;

    private String drugName;

    private String drug_Quantity;

    public String getDrugCatgory ()
    {
        return drugCatgory;
    }

    public void setDrugCatgory (String drugCatgory)
    {
        this.drugCatgory = drugCatgory;
    }

    public String getDrugName ()
    {
        return drugName;
    }

    public void setDrugName (String drugName)
    {
        this.drugName = drugName;
    }

    public String getDrug_Quantity ()
    {
        return drug_Quantity;
    }

    public void setDrug_Quantity (String drug_Quantity)
    {
        this.drug_Quantity = drug_Quantity;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [drugCatgory = "+drugCatgory+", drugName = "+drugName+", drug_Quantity = "+drug_Quantity+"]";
    }
}


