package com.example.cassandramysql.dhanuenv.daysignoff;

public class QuestionAndAnswer {
    private String questionId;

    private String answer;

    public String getQuestionId ()
    {
        return questionId;
    }

    public void setQuestionId (String questionId)
    {
        this.questionId = questionId;
    }

    public String getAnswer ()
    {
        return answer;
    }

    public void setAnswer (String answer)
    {
        this.answer = answer;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [questionId = "+questionId+", answer = "+answer+"]";
    }
}

