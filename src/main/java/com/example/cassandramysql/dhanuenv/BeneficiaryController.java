package com.example.cassandramysql.dhanuenv;

import com.example.cassandramysql.dhanuenv.beneficiary.Beneficiaries;
import com.example.cassandramysql.dhanuenv.beneficiary.BeneficiariesRepository;
import com.example.cassandramysql.dhanuenv.daysignoff.BeneficiaryData;
import com.example.cassandramysql.dhanuenv.daysignoff.DaySignOff;
import com.example.cassandramysql.dhanuenv.reports.screening.ScreeningCountsByStateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;

@RestController
@RequestMapping("/ben")
public class BeneficiaryController {

    @Autowired
    private BeneficiariesRepository beneficiariesRepository;

    @Autowired
    private ScreeningCountsByStateRepository screeningCountsByStateRepository;

    @PostMapping("new")
    public void saveBeneficiary(@RequestBody Beneficiaries beneficiaries) {

        // 1. Validate Beneficiary Record. Set any markers for invalid Record.
        // 2.
        beneficiariesRepository.save(beneficiaries);
    }

    /**
     * Deserialize the Day Sign Off JSON and update Counters.
     *
     * TODO 1. Add validations.
     * TODO 2. We have to perform all these below operations using a Batch.
     * */
    @PostMapping("poc/{state}/{year}")
    public void checkCounters(@PathVariable String state, @PathVariable String year, @RequestBody DaySignOff daySignOff) {

        //Breakpoint here to see the parsing of all data from JSON.
        ArrayList<BeneficiaryData> allBenData = daySignOff.getBeneficiaryData();

        //TODO Demonstrate Validations.

        for (BeneficiaryData b : allBenData) {
            //Extracting the required fields and update the counters accordingly.
            String gender = b.getPersonalInfo().getGender();
            String age = b.getPrelimnaryData().getAgeInYears();
            String complaints = b.getBeneficiarySummary().getChiefComplaints();

            //For debugging.
            System.out.println("Gender: " + gender);
            System.out.println("Age: " + age);
            System.out.println("Diseases: " + complaints);

            screeningCountsByStateRepository.updateCounterValue(state, year);

            //TODO increase the counts of state, zone, mandal... individually.
            if (gender.equalsIgnoreCase("Female")) {
                screeningCountsByStateRepository.updateCounterFemaleValue(state, year);
            } else {
                screeningCountsByStateRepository.updateCounterMaleValue(state, year);
            }

        }
    }




}
