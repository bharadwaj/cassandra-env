package com.example.cassandramysql.dhanuenv.reports.screening;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;


/**
 * See how the repository is extended and individual Queries are run.
 * */
public interface ScreeningCountsByStateRepository extends CassandraRepository<ScreeningCountsByState, String> {
    @Query("update screening_counts_by_state SET total_screened=total_screened+1 WHERE state= ?0 AND date_hour_screened = ?1;")
    ScreeningCountsByState updateCounterValue(String value1, String value2);

    @Query("update screening_counts_by_state SET total_female=total_female+1 WHERE state= ?0 AND date_hour_screened = ?1;")
    ScreeningCountsByState updateCounterFemaleValue(String value1, String value2);

    @Query("update screening_counts_by_state SET total_male=total_male+1 WHERE state= ?0 AND date_hour_screened = ?1;")
    ScreeningCountsByState updateCounterMaleValue(String value1, String value2);

    @Query("select * FROM screening_counts_by_state WHERE state= ?0 AND date_hour_screened = ?1;")
    ScreeningCountsByState getCountsByStateAndYear(String state, String year);


}
