package com.example.cassandramysql.dhanuenv.reports.screening;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

import java.util.Date;
import java.util.List;

public interface ScreeningCountsByZoneStateRepository extends CassandraRepository<ScreeningCountsByZoneState, String> {

    @Query("update screening_counts_by_zone_state SET total_screened=total_screened+1 WHERE state= ?0 AND zone= ?1 AND date_hour_screened = ?2;")
    ScreeningCountsByZoneState updateCounterValue(String state, String zone, Date year);

    @Query("update screening_counts_by_zone_state SET total_female=total_female+1 WHERE state= ?0 AND date_hour_screened = ?1;")
    Object updateCounterFemaleValue(String state, String year);

    @Query("update screening_counts_by_zone_state SET total_screened=total_screened+1 WHERE state= ?0 AND date_hour_screened = ?1;")
    Object updateCounterMaleValue(String state, String year);

    @Query("SELECT * FROM screening_counts_by_zone_state WHERE zone = ?0 AND state= ?1 AND date_hour_screened >= ?2 AND date_hour_screened <= ?3;")
    List<ScreeningCountsByZoneState> fetchScreenedStatastics(String zone, String state, Date start, Date end);

    @Query("SELECT sum(total_screened) FROM screening_counts_by_zone_state WHERE zone = ?0 AND state= ?1 AND date_hour_screened >= ?2 AND date_hour_screened <= ?3;")
    Object fetchScreenedCount(String zone, String state, Date start, Date end);
}

