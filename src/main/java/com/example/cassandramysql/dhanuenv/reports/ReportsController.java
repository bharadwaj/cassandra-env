package com.example.cassandramysql.dhanuenv.reports;

import com.example.cassandramysql.dhanuenv.reports.screening.ScreeningCountsByStateRepository;
import com.example.cassandramysql.dhanuenv.reports.screening.ScreeningCountsByZoneState;
import com.example.cassandramysql.dhanuenv.reports.screening.ScreeningCountsByZoneStateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/reports")
public class ReportsController {

    @Autowired
    private ScreeningCountsByStateRepository screeningCountsByStateRepository;

    @Autowired
    private ScreeningCountsByZoneStateRepository screeningCountsByZoneStateRepository;

    @GetMapping("screening/{state}/{year}")
    public ResponseEntity getCounters(@PathVariable String state, @PathVariable String year){
        return new ResponseEntity<>(screeningCountsByStateRepository.getCountsByStateAndYear(state, year), HttpStatus.OK);
    }

    @GetMapping("screening/mock/{state}/{zone}")
    public ResponseEntity mockZoneCounters(@PathVariable String state, @PathVariable String zone,  @RequestParam(value = "start", required = false) @DateTimeFormat(pattern="yyyy-MM-dd HH:mm") Date start){
        return new ResponseEntity<>(screeningCountsByZoneStateRepository.updateCounterValue(state, zone, start), HttpStatus.OK);
    }


    @GetMapping("screening/zone/{state}/{zoneId}")
    public ResponseEntity getZoneCounters(@PathVariable String zoneId,
                                          @PathVariable String state,
                                          @RequestParam(value = "start", required = false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
                                          @RequestParam(value = "end", required = false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end){

        //screeningCountsByStateRepository
        List<ScreeningCountsByZoneState> hourlyScreeningCountsByZoneStates = screeningCountsByZoneStateRepository.fetchScreenedStatastics(zoneId, state, start, end);


        return new ResponseEntity<>(hourlyScreeningCountsByZoneStates, HttpStatus.OK);
    }
}
