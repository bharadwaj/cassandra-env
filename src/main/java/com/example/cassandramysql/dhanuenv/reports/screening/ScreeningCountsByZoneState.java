package com.example.cassandramysql.dhanuenv.reports.screening;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.sql.Timestamp;
import java.util.Date;


/**
 * CREATE TABLE IF NOT EXISTS screening_counts_by_zone_state (
 * 	total_screened_0_16 counter,
 * 	male_0_16 counter,
 * 	female_0_16 counter,
 *
 * 	total_screened_6_18 counter,
 * 	male_6_18 counter,
 * 	female_6_18 counter,
 *
 * 	total_screened counter,
 * 	total_male counter,
 * 	total_female counter,
 *
 * 	awc_screened counter,
 * 	school_screened counter,
 * 	college_screened counter,
 *
 * 	state varchar,
 * 	zone varchar,
 * 	date_hour_screened timestamp,
 * 	PRIMARY KEY((zone, state), date_hour_screened)
 * 	) WITH CLUSTERING ORDER BY (date_hour_screened DESC)
 * */
@Table("screening_counts_by_zone_state")
public class ScreeningCountsByZoneState {

    private Long total_screened_0_16;
    private Long male_0_16;
    private Long female_0_16;

    private Long total_screened_6_18;
    private Long male_6_18;
    private Long female_6_18;

    private Long total_screened;
    private Long total_male;
    private Long total_female;

    private Long awc_screened;
    private Long school_screened;
    private Long college_screened;

    @PrimaryKeyColumn(name = "state", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private String state;
    @PrimaryKeyColumn(name = "zone", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private String zone;
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm", timezone="Asia/Kolkata")
    @PrimaryKeyColumn(name = "date_hour_screened", ordinal = 2, type = PrimaryKeyType.CLUSTERED, ordering = Ordering.DESCENDING)
    private Date date_hour_screened;

    public Long getTotal_screened_0_16() {
        return total_screened_0_16;
    }

    public void setTotal_screened_0_16(Long total_screened_0_16) {
        this.total_screened_0_16 = total_screened_0_16;
    }

    public Long getMale_0_16() {
        return male_0_16;
    }

    public void setMale_0_16(Long male_0_16) {
        this.male_0_16 = male_0_16;
    }

    public Long getFemale_0_16() {
        return female_0_16;
    }

    public void setFemale_0_16(Long female_0_16) {
        this.female_0_16 = female_0_16;
    }

    public Long getTotal_screened_6_18() {
        return total_screened_6_18;
    }

    public void setTotal_screened_6_18(Long total_screened_6_18) {
        this.total_screened_6_18 = total_screened_6_18;
    }

    public Long getMale_6_18() {
        return male_6_18;
    }

    public void setMale_6_18(Long male_6_18) {
        this.male_6_18 = male_6_18;
    }

    public Long getFemale_6_18() {
        return female_6_18;
    }

    public void setFemale_6_18(Long female_6_18) {
        this.female_6_18 = female_6_18;
    }

    public Long getTotal_screened() {
        return total_screened;
    }

    public void setTotal_screened(Long total_screened) {
        this.total_screened = total_screened;
    }

    public Long getTotal_male() {
        return total_male;
    }

    public void setTotal_male(Long total_male) {
        this.total_male = total_male;
    }

    public Long getTotal_female() {
        return total_female;
    }

    public void setTotal_female(Long total_female) {
        this.total_female = total_female;
    }

    public Long getAwc_screened() {
        return awc_screened;
    }

    public void setAwc_screened(Long awc_screened) {
        this.awc_screened = awc_screened;
    }

    public Long getSchool_screened() {
        return school_screened;
    }

    public void setSchool_screened(Long school_screened) {
        this.school_screened = school_screened;
    }

    public Long getCollege_screened() {
        return college_screened;
    }

    public void setCollege_screened(Long college_screened) {
        this.college_screened = college_screened;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public Date getDate_hour_screened() {
        return date_hour_screened;
    }

    public void setDate_hour_screened(Date date_hour_screened) {
        this.date_hour_screened = date_hour_screened;
    }
}
